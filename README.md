# Fork source module project

This project hosts the current state of the art of our Terraform registry module integration pipeline.

It uses [pre-commit](https://pre-commit.com/) and a galaxy of Terraform tools all provided in the reference [Terraform workspace image](https://gitlab.com/wescalefr-oss/container-images/workspaces/container_registry/eyJuYW1lIjoid2VzY2FsZWZyLW9zcy9jb250YWluZXItaW1hZ2VzL3dvcmtzcGFjZXMvdGVycmFmb3JtIiwidGFnc19wYXRoIjoiL3dlc2NhbGVmci1vc3MvY29udGFpbmVyLWltYWdlcy93b3Jrc3BhY2VzL3JlZ2lzdHJ5L3JlcG9zaXRvcnkvMTAyMDM3OC90YWdzP2Zvcm1hdD1qc29uIiwiaWQiOjEwMjAzNzh9)